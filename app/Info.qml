import QtQuick 2.4
import Ubuntu.Components 1.3

Page {
  id: aboutPage
  property int showDebug


  header: PageHeader {
    title: i18n.tr("About")
    flickable: flickable
  }

  Flickable {
    id: flickable

    anchors.fill: parent
    contentHeight: dataColumn.height + units.gu(10) + dataColumn.anchors.topMargin

    Column {
      id: dataColumn

      spacing: units.gu(3)
      anchors {
        top: parent.top; left: parent.left; right: parent.right; topMargin: units.gu(5); rightMargin:units.gu(2.5); leftMargin: units.gu(2.5)
      }

      UbuntuShape {
        width: units.gu(20)
        height: width
        anchors.horizontalCenter: parent.horizontalCenter
        source: Image {
            source: Qt.resolvedUrl("../../../graphics/uAdBlock.svg")
      }}



      Column {
        width: parent.width


        Label {
          width: parent.width
          textSize: Label.XLarge
          font.weight: Font.DemiBold
          horizontalAlignment: Text.AlignHCenter
          text: "uAdBlock"
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: i18n.tr("Version ")+ "1.5.2"
        }

        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: " "
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: i18n.tr("Developed by ")+"Malte Kiefer & Marius Gripsgard"
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: " "
        }
        Label {
        width: parent.width
        wrapMode: Text.WordWrap
        textSize: Label.Small
        horizontalAlignment: Text.AlignHCenter
        text: "<a href='https://gitlab.com/uadblock/uadblock'>https://gitlab.com/uadblock/uadblock</a>"
        onLinkActivated: Qt.openUrlExternally(link)
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: " "
        }
        Label {
        width: parent.width
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("<b>Thanks to our translators:</b>")
        }
        Label {
        width: parent.width
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text: "- Advocatux (Spanish)"
        }
        Label {
          width: parent.width
          horizontalAlignment: Text.AlignHCenter
          text: " "
        }
        Label {
        width: parent.width
        wrapMode: Text.WordWrap
        horizontalAlignment: Text.AlignHCenter
        text: i18n.tr("Special thanks to <a href='https://github.com/StevenBlack/hosts/'>StevenBlack</a> for his hosts lists")
        onLinkActivated: Qt.openUrlExternally(link)
        }
      }

    }
  }
}
