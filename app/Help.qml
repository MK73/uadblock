import QtQuick 2.4
import QtQuick.Controls 2.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem

Page {
  id: infoPage

  header: PageHeader {
      id: listHeader
      title: i18n.tr("Help")
      flickable: flickable
  }
  Flickable {
    id: flickable
    contentHeight: dataColumn.childrenRect.height
    anchors.fill: parent

    Column {
      id: dataColumn
      anchors.fill: parent
      ListItem.SingleValue {
          objectName: "InfoItem"
          height: infoColumn.childrenRect.height + units.gu(2)

          Column {
              anchors.fill: parent
              anchors.topMargin: units.gu(1)

              id: infoColumn
              spacing: units.gu(2)
              Icon {
                  id: warnIcon
                  width: parent.width/4
                  height: width
                  name: "help"
                  anchors.horizontalCenter: parent.horizontalCenter
              }
          }
      }
      ListItem.SingleValue {
      height: whatColumn.childrenRect.height + units.gu(2)
      Column {
        id: whatColumn
        anchors.fill: parent
        anchors.topMargin: units.gu(1)
        spacing: units.gu(2)
        Label {
          width: parent.width
          wrapMode: Text.Wrap
          horizontalAlignment: Text.AlignLeft
          text: i18n.tr("<b>What is uAdBlock and how it works?</b><p>uAdBlock is an adblocker based on host lists. These lists are downloaded and anchored in the system. Every system queries a domain first for an IP address in its own system, so it is possible to redirect these requests to the localhost and prevent the pages from being loaded. It does not block anything in the narrower sense, but does not loading it at all.</p>")
        }
      }

      }
      ListItem.SingleValue {
      height: listColumn.childrenRect.height + units.gu(2)
      Column {
        id: listColumn
        anchors.fill: parent
        anchors.topMargin: units.gu(1)
        spacing: units.gu(2)
        Label {
          width: parent.width
          wrapMode: Text.Wrap
          horizontalAlignment: Text.AlignLeft
          text: i18n.tr("<b>Which categories can be blocked?</b><p>Currently it is possible to block several categories:</p><br><b>Propaganda & Scams</b><p>Propaganda & Scams includes websites that are known to spread propaganda and fakenews.</p><br><b>Gambling</b><p>Gambling includes gambling websites, e.g. Casino Sites and Online Betting.</p><br><b>Porn</b><p>We do not think we have much to say :-)</p><br><b>Social</b><p>Social includes social media such as Facebook, Instagram, etc.</p>")
        }
      }

      }
      ListItem.SingleValue {
      height: stevenColumn.childrenRect.height + units.gu(2)
      Column {
        id: stevenColumn
        anchors.fill: parent
        anchors.topMargin: units.gu(1)
        spacing: units.gu(2)
        Label {
          width: parent.width
          wrapMode: Text.Wrap
          horizontalAlignment: Text.AlignLeft
          text: i18n.tr("<b>Where do the lists come from?</b><p>The lists come from the open source project of StevenBlack / Hosts. At regular intervals, these lists are updated and anyone can use and collaborate for free. Already a large number of users have contributed to these lists.</p>")
        }
      }

      }
    }

  }
}
